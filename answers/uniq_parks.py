from __future__ import print_function
import csv, sys
from collections import OrderedDict

def mapParks(row):
    return row[6]

with open(sys.argv[1], 'r') as f:
    reader = csv.reader(f)
    listTree = list(reader)

newlist = filter(lambda x: x[6] != '', listTree[1:])
parks = map(mapParks, newlist)
nameParks = OrderedDict((x,1) for x in list(parks)).keys()

for i in sorted(nameParks):
    print("%s" %(i))
