from __future__ import print_function

import sys
from operator import add

from pyspark.sql import SparkSession
from sortedcontainers import SortedList
from pyspark.sql import Row

def mapTree(tree):
    if tree[6] is None:
        return ''
    else:
        return tree[6]

if __name__ == "__main__":

    spark = SparkSession.builder.appName("PythonWordCount").getOrCreate()

    ds1 = spark.read.csv(sys.argv[1], header="true").rdd.map(mapTree).filter(lambda x: x is not '').map(
        lambda x: (x, 1)).reduceByKey(add)
    schema = spark.createDataFrame(ds1, ('parks', 'count'))
    schema.createOrReplaceTempView("Parks")
    query = spark.sql("SELECT DISTINCT parks FROM Parks ORDER BY parks asc")

    for row in query.collect():
        print ("%s" %(row["parks"]))

    spark.stop()