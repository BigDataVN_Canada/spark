from __future__ import print_function
import csv, sys
import itertools
import operator

def mapParks(row):
    return row[6]

with open(sys.argv[1], 'r') as f:
    reader = csv.reader(f)
    listTree = list(reader)

newlist = filter(lambda x: x[6] != '', listTree[1:])

parks = sorted(map(mapParks, newlist))
dict = {}
for key, group in itertools.groupby(list(parks)):
    dict[key] = len(list(group))

sorted_x = sorted(dict.items(), key=operator.itemgetter(1), reverse=True)
for k, v in list(sorted_x)[:10]:
    print("%s: %i" % (k, v))