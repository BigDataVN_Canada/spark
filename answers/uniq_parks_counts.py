from __future__ import print_function
import csv, sys
import itertools

def mapParks(row):
    return row[6]

with open(sys.argv[1], 'r') as f:
    reader = csv.reader(f)
    listTree = list(reader)

newlist = filter(lambda x: x[6] != '', listTree[1:])

parks = sorted(map(mapParks, newlist))
for key, group in itertools.groupby(list(parks)):
    print ("%s: %i" %(key, len(list(group))))

