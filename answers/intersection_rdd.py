import sys
from operator import add

from pyspark.sql import SparkSession

def mapTree(tree):
    if tree[6] is None:
        return ''
    else:
        return tree[6]


if __name__ == "__main__":

    spark = SparkSession.builder.appName("PythonWordCount").getOrCreate()

    ds1 = spark.read.csv(sys.argv[1],header= "true").rdd.map(mapTree).filter(lambda x: x is not '').map(lambda x : (x, 1)).reduceByKey(add)
    ds2 = spark.read.csv(sys.argv[2],header= "true").rdd.map(mapTree).filter(lambda x: x is not '').map(lambda x : (x, 1)).reduceByKey(add)

    outputs = ds1.cogroup(ds2).filter(lambda row: len(row[1][0]) > 0 and len(row[1][1])).sortByKey(True)

    for parks in outputs.keys().collect():
        print(parks)
    spark.stop()