from __future__ import print_function

import sys
from operator import add

from pyspark.sql import SparkSession, Row

def mapTree(tree):
    if tree[6] is None:
        return ''
    else:
        return tree[6]

if __name__ == "__main__":


    spark = SparkSession.builder.appName("PythonWordCount").getOrCreate()

    ds1 = spark.read.csv(sys.argv[1], header="true").rdd.map(mapTree).filter(lambda x: x is not '').map(lambda x: (x, 1)).reduceByKey(add)

    schema = spark.createDataFrame(ds1, ('parks', 'count'))
    schema.createOrReplaceTempView("Parks")
    query = spark.sql("SELECT * FROM Parks ORDER BY count desc LIMIT 10 ")

    for row in query.collect():
        print("%s: %i" %(row["parks"],row["count"]))
    spark.stop()




    # for (word, count) in outputs:
    #     print("%s: %i" % (word, count))
    spark.stop()