from __future__ import print_function
import csv, sys
import itertools
import operator

def mapParks(row):
    return row[6]

def createDict(path):
    with open(path, 'r') as f:
        reader = csv.reader(f)
        listTree = list(reader)

    newlist = filter(lambda x: x[6] != '', listTree[1:])

    parks = sorted(map(mapParks, newlist))
    dict = {}
    for key, group in itertools.groupby(list(parks)):
        dict[key] = len(list(group))
    return dict

dict1 = createDict(sys.argv[1])
dict2 = createDict(sys.argv[2])

for row in sorted(dict1.keys()):
    if row in dict2:
        print(row)
