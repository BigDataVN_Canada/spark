from __future__ import print_function

import sys
from operator import add

from pyspark.sql import SparkSession

def mapTree(tree):
    if tree[6] is None:
        return ''
    else:
        return tree[6]

if __name__ == "__main__":

    spark = SparkSession.builder.appName("PythonWordCount").getOrCreate()

    lines = spark.read.csv(sys.argv[1], header= "true").rdd.map(mapTree).filter(lambda x: x is not '')
    count = lines.map(lambda x : (x, 1)).reduceByKey(add).sortByKey(True)
    outputs = count.collect()
    for (word, count) in outputs:
        print("%s: %i" % (word, count))
    spark.stop()