from __future__ import print_function

import sys
from operator import add

from pyspark.sql import SparkSession
from sortedcontainers import SortedList
from pyspark.sql import Row

def mapTree(tree):
    if tree[6] is None:
        return ''
    else:
        return tree[6]

if __name__ == "__main__":

    spark = SparkSession.builder.appName("PythonWordCount").getOrCreate()

    lines = spark.read.csv(sys.argv[1]).rdd.map(mapTree).filter(lambda x: x is not '')
    count = lines.map(lambda x : (x, 1)).reduceByKey(add)
    outputs = count.keys().collect()
    outputs = SortedList(outputs)
    for (word) in outputs:
        print("%s " % (word))
    spark.stop()