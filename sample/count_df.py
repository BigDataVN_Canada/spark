from __future__ import print_function

import sys
from operator import add

from pyspark.sql import SparkSession
from pyspark.sql import Row



def mapTree(tree):
    if tree[6] is None:
        return ''
    else:
        return tree[6]

if __name__ == "__main__":

    spark = SparkSession.builder.appName("PythonWordCount").getOrCreate()

    ds1 = spark.read.csv(sys.argv[1], header= "true").rdd.map(mapTree).filter(lambda x: x is not '')
    df1 = ds1.map(lambda row: Row(parks=row))

    schema = spark.createDataFrame(df1)
    schema.createOrReplaceTempView("Parks")
    query = spark.sql("SELECT count(parks) FROM Parks")
    # query = schema.select((count("parks")) True)
    print(query.collect()[0][0])
    spark.stop()