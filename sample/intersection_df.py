import sys
from operator import add

from pyspark.sql import SparkSession

def mapTree(tree):
    if tree[6] is None:
        return ''
    else:
        return tree[6]

if __name__ == "__main__":
    spark = SparkSession.builder.appName("PythonWordCount").getOrCreate()

    ds1 = spark.read.csv(sys.argv[1],header= "true").rdd.map(mapTree).filter(lambda x: x is not '').map(lambda x : (x, 1)).reduceByKey(add)
    ds2 = spark.read.csv(sys.argv[2],header= "true").rdd.map(mapTree).filter(lambda x: x is not '').map(lambda x : (x, 1)).reduceByKey(add)

    df1 = spark.createDataFrame(ds1, ('parks','count'))
    df2 = spark.createDataFrame(ds2, ('parks','count'))

    df1.createTempView('df1')
    df2.createTempView('df2')

    # df1.join(df2, df1.parks == df2.parks, how='inner')
    outputs = spark.sql('SELECT df1.parks FROM df1 JOIN df2 ON df1.parks == df2.parks')
    # parksName = outputs.rdd.map(lambda park: park.parks).collect()
    # for name in parksName:
    #     print(name)
    # outputs = df1.collect()
    outputs.show()
    for row in outputs.collect():
        print(row["parks"])

    spark.stop()