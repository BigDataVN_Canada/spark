# import sys
# from pyspark import SparkContext
# from pyspark.sql import SparkSession
# import os
#
# def mapTreesInParks(tree):
#     if tree['Nom_parc'] is not None:
#         return 1
#     else:
#         return 0
#
# sc = SparkContext("local","parks_rdd")
# session = SparkSession.builder.appName("parks_rdd").getOrCreate
#
#
#
# data = session.read.csv(sys.argv[1], header= "true").rdd
#
# count = data.map(mapTreesInParks).reduce(lambda a, b: a +b)
#
# print(count)
# logFile = "../README.md"  # Should be some file on your system
# sc = SparkContext("local", "Simple App")
# logData = sc.textFile(logFile).cache()
#
# numAs = logData.filter(lambda s: '0' in s).count()
# numBs = logData.filter(lambda s: 'b' in s).count()
#
# print ("Lines with a: %i, lines with b: %i" % (numAs, numBs))
from __future__ import print_function

import sys
from operator import add

from pyspark.sql import SparkSession


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: wordcount <file>", file=sys.stderr)
        exit(-1)

    spark = SparkSession.builder.appName("PythonWordCount").getOrCreate()

    lines = spark.read.csv(sys.argv[1], header="true").rdd.map(lambda r: r[0])
    print(lines)
    counts = lines.flatMap(lambda x: x.split(' ')).map(lambda x: (x, 1)).reduceByKey(add)
    output = counts.collect()
    for (word, count) in output:
        print("%s: %i" % (word, count))

    spark.stop()