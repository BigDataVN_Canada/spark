from __future__ import print_function

import sys
from operator import add

from pyspark.sql import SparkSession, Row


def mapTree(tree):
    if tree[6] is None:
        return ''
    else:
        return tree[6]


if __name__ == "__main__":

    spark = SparkSession.builder.appName("PythonWordCount").getOrCreate()

    lines = spark.read.csv(sys.argv[1], header= "true").rdd.map(mapTree).filter(lambda x: x is not '')
    # count = lines.map(lambda x : (x, 1)).reduceByKey(add)
    # outputs = count.collect()
    # for (word, count) in outputs:
    #     print("%s: %i" % (word, count))
    df1 = lines.map(lambda row: Row(parks=row, count=1))
    schema = spark.createDataFrame(df1)
    schema.createOrReplaceTempView("Parks")

    query = spark.sql("SELECT parks, count(parks) FROM Parks GROUP BY parks")
    for row in query.collect():
        print("%s : %i " % (row[0], row[1]))

    spark.stop()